import { IContextValue } from "@/@types";
import { createContext } from "react";

export const context = createContext<IContextValue>({} as IContextValue);
