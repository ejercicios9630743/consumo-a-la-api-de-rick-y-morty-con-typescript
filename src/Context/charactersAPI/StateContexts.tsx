//src\Context\charactersAPI\StateContexts.tsx
import React, { useReducer } from "react";
import { reducer } from "./ReducerContext";
import { context } from "./CreateContexts";
import axios, { AxiosResponse } from "axios";
import { IContextValue, IEstructDataApiCharacters, IProps, IInicialState } from '../../@types/index';

const StateContexts: React.FC<IProps> = ({ children }) => {
  const inicialState: IInicialState = {
    charactersGlobal: [],
    info: [],
    pageActual: [],
    characterOnly: [],
    characterFavorites: [],
    errorLoad: null
  }
  const [state, dispatch] = useReducer(reducer, inicialState)
  

  let filterUrl: string | null = `${process.env.NEXT_PUBLIC_API_URL}/api/character`
  let data: IEstructDataApiCharacters

  const fetchCharacterGlobal: IContextValue['fetchCharacterGlobal'] = async (Url, numTheCharacters) => {
    if(Url !== filterUrl + '/?page=[]'){
      filterUrl = Url
      await axios.get(filterUrl ? filterUrl : '').then((response: AxiosResponse<IEstructDataApiCharacters>)=>{
      data = response.data
    })
    }
    
    const pageActualNum = data.info.prev ? Number.parseInt(data.info.prev.split('=')[1]) + 1 : 1;

    if(numTheCharacters) {
      data.results = data.results.filter((item) => item.id <= numTheCharacters)
    }

    dispatch({
      payload: data.results,
      info: data.info,
      pageActual: pageActualNum,
      type: `characterGlobal`
    })
  }

  const fetchCharacterOnly: IContextValue['fetchCharacterOnly'] = async (id) => {
    await axios.get(filterUrl + `/${id}`).then((response: AxiosResponse<IEstructDataApiCharacters>)=>{
      data = response.data
    })
    dispatch({
      payload: data,
      errorLoad: data.error,
      type: 'characterOnly'
    })
  }

  const charactersFavorites: IContextValue['charactersFavorites'] = (chara, isDelete) => {
    if(isDelete){
      dispatch({
        characterFavoritesLoad: chara,
        type: 'characterFavoritesDelete'
      })
    }else{
      dispatch({
        characterFavoritesLoad: chara,
        type: 'characterFavorites'
      })
    }
    
  }

  const contextValue: IContextValue = {
    allCharacters: state.charactersGlobal,
    info: state.info,
    pageActual: state.pageActual,
    onlyPj: state.characterOnly,
    errorLoad: state.errorLoad,
    characterFavorites: state.characterFavorites,
    fetchCharacterGlobal,
    fetchCharacterOnly,
    charactersFavorites
  };

  return (
    <context.Provider value={contextValue}>
      {children}
    </context.Provider>
  )
}


export default StateContexts