
import { IApiElements, IInicialState } from "@/@types"


export const reducer = (state: any, action: IInicialState) => {
  const {payload, info, pageActual, errorLoad, type, characterFavoritesLoad} = action
  switch(type){
    case 'characterGlobal':
      return {
        ...state,
        charactersGlobal: payload,
        info: info,
        pageActual: pageActual
      }
      case 'characterOnly':
      return {
        ...state,
        characterOnly: payload,
        errorLoad: errorLoad
      }
      case 'characterFavorites':
        const { characterFavorites: charactersFavoritesOld } = state;
        if(characterFavoritesLoad) {
          const filterNoDuplicateCharacters: IApiElements[] = charactersFavoritesOld.filter((item:IApiElements)=> item.id !== characterFavoritesLoad.id)
          const newValues = [...filterNoDuplicateCharacters, characterFavoritesLoad]
          return {
            ...state,
            characterFavorites: newValues,
          };
        }
      case 'characterFavoritesDelete':
        const { characterFavorites: charaStateForDeleteCharacter } = state;
        const favoritesAfterDeleteCharacter = charaStateForDeleteCharacter?.filter((item: IApiElements) => item !== characterFavoritesLoad);
        return {
          ...state,
          characterFavorites: favoritesAfterDeleteCharacter || [],
        };
      }
    }