// These styles apply to every route in the application
import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import {NextUIProvider} from "@nextui-org/react";
import {ThemeProvider as NextThemesProvider} from "next-themes";
import StateContexts from '@/Context/charactersAPI/StateContexts';
import { useRouter } from 'next/router';
import Layout from '@/components/Layout';
export default function App({ Component, pageProps }: AppProps) {
  const router = useRouter()
  return (
    <NextUIProvider navigate={router.push}>
      <NextThemesProvider attribute='class' defaultTheme='dark'>
        <StateContexts>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </StateContexts>
      </NextThemesProvider>
    </NextUIProvider>
  )
}