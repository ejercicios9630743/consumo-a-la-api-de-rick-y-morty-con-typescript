
import Grid from "@/components/Grid"
import CharactersElements from "@/components/CharactersElements"
import { Loader } from "@/components/Loader"
import { useContext, useEffect, useState } from "react"
import { context } from "@/Context/charactersAPI/CreateContexts"
import { Button, ButtonGroup, Pagination } from "@nextui-org/react"
import { TFilterUrl } from "@/@types"
function Characters() {
  const {allCharacters , info, pageActual, fetchCharacterGlobal} = useContext(context)
  const [filterUrl, setFilterUrl] = useState<TFilterUrl>(`${process.env.NEXT_PUBLIC_API_URL}/api/character/?page=${pageActual}`)
  const [isLoading, setIsLoading] = useState(true)

  const assignUrlFilter = (num:number | ((num: number) => number)) => {setFilterUrl(`${process.env.NEXT_PUBLIC_API_URL}/api/character/?page=${num}`)}

  useEffect(() => {
    const fetchData = async () => {
      await fetchCharacterGlobal(filterUrl);
      setIsLoading(false);
    };
  
     fetchData();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filterUrl]);
  
  
  if(isLoading){
    return (
      <>
      <div className={'text-center font-sans font-bold my-6 text-3xl'}>
          <h1>Characters</h1>
        </div>
        <Loader/>
      </>
    )
  }else {
      return (
        <>
          <h1 className="text-center font-sans font-bold my-6 text-3xl">Characters</h1>
          <Grid elements={allCharacters} component={CharactersElements}/>
          <div className="grid grid-rows-3 place-items-center gap-y-2 my-2">
            <p className="text-small text-center text-default-500">Selected Page: {pageActual}</p>
            <Pagination
              total={info.pages}
              color="secondary"
              variant="light"
              page={pageActual}
              size="sm"
              onChange={assignUrlFilter}
              className="m-0 p-0"
              siblings={2}
            />
            <ButtonGroup variant="ghost" color="primary" size={'sm'}>
              <Button onPress={() =>{setFilterUrl(`${process.env.NEXT_PUBLIC_API_URL}/api/character/?page=1`)}} disabled={!info.prev}>First page</Button>
              <Button onPress={() =>{setFilterUrl(info.prev)}} isDisabled={!info.prev}>Prev</Button>
              <Button onPress={() =>{setFilterUrl(info.next)}} isDisabled={!info.next}>Next</Button>
              <Button onPress={() =>{setFilterUrl(`${process.env.NEXT_PUBLIC_API_URL}/api/character/?page=${info.pages}`)}} disabled={!info.next}>Last page</Button>
            </ButtonGroup>
          </div>
        </>
        
  )
  }
}




export default Characters