import { context } from '@/Context/charactersAPI/CreateContexts';
import React, { useCallback, useState } from 'react'
import { useContext } from 'react';
import {Table, TableHeader, TableColumn, TableBody, TableRow, TableCell, User, Chip, Tooltip, ChipProps, Button, Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, useDisclosure, Pagination} from "@nextui-org/react";
import { IApiElements } from '@/@types';
import OnlyCharacterElement from '@/components/OnlyCharacterElement';
import { MdPreview } from "react-icons/md";
import { MdDelete } from "react-icons/md";


const Favorites: React.FC = () => {
  const { characterFavorites, charactersFavorites} = useContext(context)
  const {isOpen, onOpen, onOpenChange} = useDisclosure();
  const [characterFavoriteElementForModal, setCharacterFavoriteElementForModal] = useState<IApiElements>({} as IApiElements);
  const [pageForPaginationTable, setPageForPaginationTable] = React.useState(1);
  const rowsPerPage = 10;

  const totalPagesForPaginationTable = Math.ceil(characterFavorites.length / rowsPerPage);

  const itemsRenderCharacterFavoritesTableRows = React.useMemo(() => {
    const start = (pageForPaginationTable - 1) * rowsPerPage;
    const end = start + rowsPerPage;

    return characterFavorites.slice(start, end);
  }, [pageForPaginationTable, characterFavorites]);

  const statusColorMap: Record<string, ChipProps["color"]>  = {
    Alive: "success",
    Dead: "danger",
    unknown: "default",
  };

  const columns = [
    {name: "name", uid: "Name"},
    {name: "gender", uid: "Gender"},
    {name: "status", uid: "Status"},
    {name: "species", uid: "Specie"},
    {name: "actions", uid: "Actions"},
  ];

  const renderCell = useCallback((itemCharacter: any, columnKey: React.Key) => {

    switch (columnKey) {
      case "name":
        return (
          <User
            avatarProps={{size:'lg' ,radius: "lg", src: itemCharacter.image}}
            description={itemCharacter.type}
            name={itemCharacter.name}
          >
            {itemCharacter.name}
          </User>
        );
      case "gender":
        return (
            <span className="text-bold text-sm capitalize">{itemCharacter.gender}</span>
        );
      case "status":
        return (
          <Chip className="capitalize" color={statusColorMap[itemCharacter.status]} size="sm" variant="flat">
            {itemCharacter.status}
          </Chip>
        );
      case "species":
        return (
          <Chip className={`capitalize ${itemCharacter.species === 'Human' ? 'bg-yellow-500' :
          itemCharacter.species === 'Robot' ? 'bg-indigo-500' :
          itemCharacter.species === 'Alien' ? 'bg-teal-500' :
          itemCharacter.species === 'Humanoid' ? 'bg-amber-500' :
          itemCharacter.species === 'Mythological Creature' ? 'bg-purple-500' :
          itemCharacter.species === 'Cronenberg' ? 'bg-blue-500' :
          'bg-gray-500'}` } 
          size="sm" 
          variant="flat"
          >
            {itemCharacter.species}
          </Chip>
        )
      case "actions":
        return (
          <>
          <div className="relative flex items-center gap-2">
            <Tooltip content="See Character">
              <div className="text-2xl text-default-400 cursor-pointer active:opacity-50" onClick={() => {onOpen(); setCharacterFavoriteElementForModal(itemCharacter)}}>
              <MdPreview/>
              </div>
            </Tooltip>
            <Tooltip color="danger" content="Delete Character">
              <div className="text-2xl text-danger cursor-pointer active:opacity-50" onClick={()=> {charactersFavorites(itemCharacter, true)}}>
                <MdDelete/>
              </div>
            </Tooltip>
          </div>
          
        </>
        );
      default:
        return itemCharacter;
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])

  return (
    <>
        <h1 className='text-center font-sans font-bold my-6 text-3xl'>Favorites</h1>
        <div className='mx-auto w-3/5'>
          <Table 
        aria-label="Example table with dynamic content" 
        bottomContent={
          <div className='flex w-full justify-center'>
            <Pagination
            isCompact
            showControls
            showShadow
            color='secondary'
            page={pageForPaginationTable}
            total={totalPagesForPaginationTable}
            onChange={(page) => setPageForPaginationTable(page)}
            />
          </div>
        }
        >
          <TableHeader columns={columns}>
            {(column) => <TableColumn key={column.name}>{column.uid}</TableColumn>}
          </TableHeader>
        <TableBody items={itemsRenderCharacterFavoritesTableRows}>
          {(item) => (
              <TableRow key={item.id}>
              {(columnKey) => <TableCell>{renderCell(item, columnKey)}</TableCell>}
            </TableRow>
          )}
        </TableBody>
      </Table>
        </div>
    <Modal backdrop={"blur"} isOpen={isOpen} onOpenChange={onOpenChange} placement="center" scrollBehavior="outside">
            <ModalContent>
                 {(onClose) => (
                   <>
                     <ModalHeader className="flex flex-col">{characterFavoriteElementForModal.name}</ModalHeader>
                     <ModalBody>
                       <OnlyCharacterElement elements={characterFavoriteElementForModal}/>
                     </ModalBody>
                     <ModalFooter>
                       <Button color="danger" variant="light" onPress={onClose}>
                         Close
                       </Button>
                     </ModalFooter>
                   </>
                 )}
            </ModalContent>
          </Modal>
    </>
      
  
  );
}

export default Favorites