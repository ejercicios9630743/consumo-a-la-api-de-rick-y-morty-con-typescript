import Image from 'next/image'

export const Loader = () => {
  return (
    <div className={'my-0 mx-auto relative top-1/4'}>
        <Image src={'/img/rick-morty-in-pc.png'} width={500} height={500} priority={true} alt={'Rick y Morty en un pc'} className={'w-full max-w-lg h-auto'}/>
    </div>
  )
}
