import { Tooltip } from "@nextui-org/react";
import { Image, useDisclosure } from "@nextui-org/react";
import { Card, CardBody, CardFooter } from '@nextui-org/react';
import {Modal, ModalContent, ModalHeader, ModalBody, ModalFooter, Button} from "@nextui-org/react";
import NextImage from 'next/image';
import OnlyCharacterElement from "./OnlyCharacterElement";
import { MdOutlineFavorite } from "react-icons/md";
import { context } from "@/Context/charactersAPI/CreateContexts";
import { useContext } from 'react';
import { MdOutlineFavoriteBorder } from "react-icons/md";
import { IPropsCharactersElements } from "@/@types";


const CharactersElements: React.FC<IPropsCharactersElements> =({elements}) => {
  const { id, name, status, species, image } = elements;
  const {isOpen, onOpen, onOpenChange} = useDisclosure();
  const {characterFavorites, charactersFavorites} = useContext(context)
  const stylesIfStatusIs: string = `${status === 'Alive' ? 'bg-green-500' : status === 'Dead' ? 'bg-red-500': status === 'unknown' ? 'bg-slate-500' : ''}`;
  const isListedFavoriteCharacter = characterFavorites.some((item) => item.id === id)

    

  const speciesBackground: string = 
  species === 'Human' ? 'bg-yellow-500' :
  species === 'Robot' ? 'bg-indigo-500' :
  species === 'Alien' ? 'bg-teal-500' :
  species === 'Humanoid' ? 'bg-amber-500' :
  species === 'Mythological Creature' ? 'bg-purple-500' :
  species === 'Cronenberg' ? 'bg-blue-500' :
  'bg-gray-500';


  return (
    <div onClick={() => {onOpen()}} className="hover:opacity-100 transition-none cursor-pointer">
     <Card
       isHoverable={true}
       className="border-none font-sans transition-colors [&_p]:hover:text-primary w-80"
       radius="md"
     >
      <CardBody>
      <Image as={NextImage} priority={true} width={300} height={300} className={'rounded-lg'} src={image} alt={`imagen del personaje ${name}`}/>
      </CardBody>

       <CardFooter className="flex flex-col">
          <h1 className={'capitalize font-extrabold text-2xl text-wrap text-center'}>{name}</h1>
          <div className={'flex flex-row gap-4 my-2'}>
            <span className={`px-2 rounded-xl capitalize font-medium ` + stylesIfStatusIs} id={"estado"}>{status}</span>
            <span className={`px-2 rounded-xl capitalize font-medium ` + speciesBackground} id={"especies"}>{species}</span>
          </div>
          <Tooltip content={'Add character to favorites list'}>
            <Button size={'sm'} radius="full" className={'bg-rose-600 [&_svg]:w-5 [&_svg]:h-5'} onPress={() => charactersFavorites(elements, isListedFavoriteCharacter)}>{isListedFavoriteCharacter ? <MdOutlineFavorite/> : <MdOutlineFavoriteBorder/>}</Button>
          </Tooltip>
          <p className={`font-semibold text-lg text-blue-700`}>More</p>
       </CardFooter>
     </Card>
     <Modal backdrop={"blur"} isOpen={isOpen} onOpenChange={onOpenChange} placement="center" scrollBehavior="outside">
     <ModalContent>
          {(onClose) => (
            <>
              <ModalHeader className="flex flex-col">{name}</ModalHeader>
              <ModalBody>
                <OnlyCharacterElement elements={elements}/>
              </ModalBody>
              <ModalFooter>
                <Button color="danger" variant="light" onPress={onClose}>
                  Close
                </Button>
              </ModalFooter>
            </>
          )}
        </ModalContent>
     </Modal>
    </div>
  )
  }

export default CharactersElements