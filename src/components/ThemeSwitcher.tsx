// components/ThemeSwitcher.tsx
import {useTheme} from "next-themes";
import { Button } from "@nextui-org/react";
import { FaMoon } from "react-icons/fa";
import { FaSun } from "react-icons/fa";
import { useState, useEffect } from "react";

export const ThemeSwitcher: React.FC = () => {
  const {theme ,setTheme } = useTheme()
  const [mounted, setMounted] = useState(false)


  useEffect(() => {
    setMounted(true)
  }, [])

  if(!mounted){
    return null
  }

  return (
    <div>
      <Button size="sm" color="primary" variant="ghost" onClick={()=> setTheme(theme === 'dark' ? 'light' : 'dark')}>{theme === 'dark' ? <FaMoon/> : theme === 'light' ? <FaSun/> : <FaMoon/>}</Button>
    </div>
  )
};