import React from 'react'
import Link from "next/link";
import { Button, Image } from '@nextui-org/react';
import {  Navbar as NavbarNextUI,   NavbarBrand,   NavbarContent,   NavbarItem, NavbarMenuToggle, NavbarMenu, NavbarMenuItem} from "@nextui-org/react";
import { ThemeSwitcher } from './ThemeSwitcher';
import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import NextImage from "next/image";
import { MdOutlineFavorite } from "react-icons/md";
const Navbar: React.FC = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const [isLoadingFavorites, setIsLoadingFavorites] = useState(false);
  const router = useRouter()

  useEffect(() => {
    const handleRouteChangeStart = (url: string) => {
      if (url === '/favorites') {
        setIsLoadingFavorites(true);
      }
    };

    const handleRouteChangeComplete = (url: string) => {
      if (url === '/favorites') {
        setIsLoadingFavorites(false);
      }
    };

    router.events.on('routeChangeStart', handleRouteChangeStart);
    router.events.on('routeChangeComplete', handleRouteChangeComplete);

    return () => {
      router.events.off('routeChangeStart', handleRouteChangeStart);
      router.events.off('routeChangeComplete', handleRouteChangeComplete);
    };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const irAFavoritos = () => {
    router.push("/favorites")
  }
  return (
    <>
    <NavbarNextUI isBordered={true} onMenuOpenChange={setIsMenuOpen}>
      <NavbarContent>
        <NavbarMenuToggle
          aria-label={isMenuOpen ? "Close menu" : "Open menu"}
          className="sm:hidden"
        />
        <NavbarBrand>
          <Link href='/' className='w-16 sm:w-2/12 hover:content-changeLogo'>
            <Image as={NextImage} priority={true} src={'/img/morty-white.png'} width={100} height={100} alt="logo-white" className={'hidden dark:block w-full h-auto'}/>
            <Image as={NextImage} priority={true} src={'/img/logo2.png'} width={100} height={100} alt="logo-black" className={'block dark:hidden w-full h-auto'}/>
          </Link>
          </NavbarBrand>
      </NavbarContent>

      <NavbarContent className="hidden sm:flex gap-4" justify="center">
        <NavbarItem>
          <Link color="foreground" href="/characters" className='text-large font-bold font-sans hover:text-primary'>Characters</Link>
        </NavbarItem>
        <NavbarItem>
        <Button
            onPress={irAFavoritos}
            size="sm"
            className="bg-rose-600"
            endContent={<MdOutlineFavorite />}
            disabled={isLoadingFavorites}
            isLoading={isLoadingFavorites}
          >Favorites</Button>
        </NavbarItem>
      </NavbarContent>
      <NavbarContent justify="end">
        <ThemeSwitcher></ThemeSwitcher>
      </NavbarContent>
      <NavbarMenu>
          <NavbarMenuItem>
            <Link color="foreground" href="/characters" className='text-large font-bold font-sans hover:text-primary'>Characters</Link>
          </NavbarMenuItem>
          <NavbarItem>
            <ThemeSwitcher></ThemeSwitcher>
          </NavbarItem>
          <NavbarItem>
          <Button onPress={irAFavoritos} size='sm' className='bg-rose-600' endContent={<MdOutlineFavorite/>}>Favorites</Button>
          </NavbarItem>
      </NavbarMenu>
    </NavbarNextUI>
    </>
  )
}

export default Navbar