import React from 'react'
import { IApiElements, IGridProps, IPropsCharactersElements, IPropsOnlyCharacters } from '@/@types'

const Grid: React.FC<IGridProps> = ({elements, component: Component}) => {
    return (
      <article className={'flex flex-wrap justify-center w-10/12 gap-3 mx-auto my-0'}>
        {elements.map((item: IApiElements) => {
        return (
          <Component key={item.id} elements={item}/>
        )
      })}
      </article>
    )
}

export default Grid