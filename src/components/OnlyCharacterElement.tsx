import { Link } from "@nextui-org/react";
import { Image } from "@nextui-org/react";
import { formatearFecha } from "../../helpers";
import NextImage from 'next/image';
import { IPropsOnlyCharacters } from "@/@types";

const OnlyCharacterElement: React.FC<IPropsOnlyCharacters> = ({elements}) => {
  const { name, status, species, type, gender, origin, location, episode, image, created } = elements;
  return (
      <div className="h-min grid grid-rows-2 place-items-center gap-y-2">
        <Image as={NextImage} isBlurred={true} priority={true} src={image} alt={`imagen del personaje ${name}`} width={300} height={300} className={'rounded-xl my-auto mx-0'}/>
        <div className="flex flex-row gap-4">
          <div className={'[&_span]:text-xl [&_span]:text-primary [&_span]:drop-shadow-sm [&_span]:font-black'}>
            <span className={''}>Nombre:</span> <p>{name}</p>
            <span className={''}>Genero:</span> <p>{gender}</p>
            <span className={''}>Estado:</span> <p>{status}</p>
            <span className={''}>Origen:</span> <p>{origin.name}</p>
            <span className={''}>Localización:</span> <p>{location.name}</p>
            <span className={''}>Creado en:</span> <p>{formatearFecha(created)}</p>
          </div>
          <div className={'[&_span]:text-xl [&_span]:text-primary [&_span]:drop-shadow-sm [&_span]:font-black'}>
            <span className={''}>Especie: </span> <p>{species}</p>
            <span className={''}>Tipo:</span> <p>{type ? type : `N/A`}</p>
            <span className={''}>Episode:</span> <div className={'flex flex-col overflow-y-auto overflow-x-hidden max-h-40 min-h-unit-4 my-1 mx-0 pr-1 [&_a]:w-max [&_a]:no-underline [&_a]:pr-3'}>{episode.map((item: string,index: number)=> {const numEpisode = item.split('/'); return (<Link key={index} href={item}>{'Episodio ' + numEpisode[5]}</Link>)})}</div>
          </div>
        </div>
      </div>
  )
}

export default OnlyCharacterElement