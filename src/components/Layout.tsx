import Head from "next/head";
import React from 'react'
import {ReactNode} from 'react'
import Header from "./Header";
interface Props {
  children: ReactNode
}

const Layout: React.FC<Props> = ({children}) => {
  return (
    <>
      <Head>
        <title>{`Rick and Morty`}</title>
      </Head>
      <Header/>
      <main className="flex flex-col min-h-77 py-3 px-0">
        {children}
      </main>
      
    </>
  )
}

export default Layout