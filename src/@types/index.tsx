import { ReactNode } from "react"

export interface IApiElements {
  id: number,
  name: string,
  status: string,
  species: string,
  type: string,
  gender: string,
  origin: {
    name: string,
    url: string
  },
  location: {
    name: string,
    url: string
  },
  image: string,
  episode: string[],
  url: string,
  created: string
}

export interface IProps {
  children: ReactNode
}

export interface IEstructDataApiCharacters {
  info: {
    count: number,
    pages: number,
    next: string | null,
    prev: string | null
  },
  results: IApiElements[],
  error?: string | null
}

export interface IContextValue {
  allCharacters: IApiElements[];
  info: {
    count: number;
    pages: number;
    next: string | null;
    prev: string | null;
  };
  pageActual: number;
  onlyPj: IApiElements;
  errorLoad: string;
  characterFavorites: [] | IApiElements[]
  fetchCharacterGlobal: (Url: string | null, numTheCharacters?: TRange1To20) => Promise<void>
  fetchCharacterOnly: (id: string | string[] | null) => Promise<void>
  charactersFavorites: (chara: IApiElements, isDelete: boolean) => void
}

export interface IInicialState {
  payload?: IEstructDataApiCharacters | IApiElements[] | IApiElements
  charactersGlobal?: [] | IApiElements[],
  info?: [] | {
    count: number;
    pages: number;
    next: string | null;
    prev: string | null;
  },
  pageActual?: [] | number,
  characterOnly?: [] | IApiElements,
  characterFavorites?: [] | IApiElements[]
  characterFavoritesLoad?: IApiElements
  errorLoad?: null | IContextValue["errorLoad"] | undefined
  type?: string
}

export interface IPropsOnlyCharacters {
  elements: IApiElements
}

export interface IPropsCharactersElements {
  elements: IApiElements
}

export type TFilterUrl = string | null;

export interface IGridProps {
  elements: IApiElements[],
  component: React.FC<IPropsOnlyCharacters> | React.FC<IPropsCharactersElements>
}

export type TRange1To20 = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20;
