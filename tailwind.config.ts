import type { Config } from 'tailwindcss'
import { nextui } from "@nextui-org/react";

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      content: {
        changeLogo: 'url("../../public/img/morty.png")'
      },
      width: {
        'auto': "auto"
      },
      height: {
        'auto': "auto"
      },
      colors: {
        paletteRickAndMorty: {
          100: "#F5FCCD",
          200: "#78D6C6",
          300: "#419197",
          400: "#12486B"
        },
        'laser-lemon': {
        '50': '#fbfde9',
        '100': '#fafdc4',
        '200': '#f8fb8d',
        '300': '#f9f759',
        '400': '#f4e91b',
        '500': '#e4cf0e',
        '600': '#c5a409',
        '700': '#9d760b',
        '800': '#825e11',
        '900': '#6f4c14',
        '950': '#412907',
      },
      'pastel-green': {
        '50': '#f3fcf1',
        '100': '#e1fbdd',
        '200': '#c3f4be',
        '300': '#81e777',
        '400': '#5bd850',
        '500': '#35be29',
        '600': '#269d1c',
        '700': '#227b1a',
        '800': '#1e6219',
        '900': '#1a5017',
        '950': '#092c07',
        },
      },
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
      boxShadow: {
        neoGreenOff: 'rgba(72, 255, 0, 0.17) 0px -23px 25px 0px inset, rgba(106, 255, 0, 0.15) 0px -36px 30px 0px inset, rgba(64, 255, 0, 0.1) 0px -79px 40px 0px inset, rgba(98, 255, 0, 0.06) 0px 2px 1px, rgba(106, 255, 0, 0.09) 0px 4px 2px, rgba(157, 255, 0, 0.09) 0px 8px 4px, rgba(115, 255, 0, 0.09) 0px 16px 8px, rgba(132, 255, 0, 0.09) 0px 32px 16px',
        neoGreenOn: 'rgba(72, 255, 0, 0.27) 0px -23px 25px 0px inset, rgba(106, 255, 0, 0.25) 0px -36px 30px 0px inset, rgba(64, 255, 0, 0.11) 0px -79px 40px 0px inset, rgba(98, 255, 0, 0.16) 0px 2px 1px, rgba(106, 255, 0, 0.19) 0px 4px 2px, rgba(157, 255, 0, 0.19) 0px 8px 4px, rgba(115, 255, 0, 0.19) 0px 16px 8px, rgba(132, 255, 0, 0.19) 0px 32px 16px'
      },
      minHeight: {
        77: '77vh'
      }
    },
  },
  darkMode: "class",
  plugins: [nextui({
    addCommonColors: false
  })],
}
export default config
